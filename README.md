# Kata compte bancaire

## Exercice
Réalisez le code en C#, dotnet core permettant d'implémenter les US ci-dessous de façon simple en utilisant toutes les pratiques qui vous semblent pertinentes pour réaliser un code professionnel de qualité.    
    
Pensez à votre compte bancaire personnel.     
Quand vous avez un doute, allez vers la solution la plus **simple**.  
    
Placez votre code sur un dépôt git publique.    
  
## Besoins

Dépot et retrait d'argent    
Relevé de compte (date, montant, balance)       
Impression relevé de compte     

## User Stories    

### US 1:
**Afin de** mettre de l'argent de côté   
**En tant que** client d'une banque   
**Je voudrais** faire un dépôt sur mon compte    

### US 2:
**Afin d'** enlever un peu ou toutes mes économies      
**En tant que** client d'une banque      
**Je voudrais** faire un retrait sur mon compte bancaire       

### US 3:
**Afin de** vérifier les opérations que j'ai déjà effectuées    
**En tant que** client d'une banque    
**Je voudrais** voir l'historique (opérations, date, montant, balance) de mes opérations    


---
---
---
    

# Bank account kata

Think of your personal bank account experience When in doubt, go for the **simplest** solution

## Requirements

Deposit and Withdrawal    
Account statement (date, amount, balance)    
Statement printing    

## User Stories    

### US 1:
**In order** to save money    
**As** a bank client    
**I want to** make a deposit in my account    

### US 2:
**In order to** retrieve some or all of my savings    
**As** a bank client    
**I want to** make a withdrawal from my account    

### US 3:
**In order to** check my operations    
**As** a bank client    
**I want to** see the history (operation, date, amount, balance) of my operations    

